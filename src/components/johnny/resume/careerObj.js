import React from 'react'

export default function careerObj() {
  return (
    <div className="my-10">
        <h1 className="text-xl font-bold ">CAREER OBJECTIVE</h1>
        <p> As a current student pursuing a degree in Information Technology. I'm seeking experience to apply and further develop my technical and soft skills in a real-world setting. I aim to apply my knowledge and abilities to be a master frontend developer. And I am committed to diligently honing my skills, developing my abilities, and actively seeking new knowledge through continuous learning.</p>
    </div>
  )
}
