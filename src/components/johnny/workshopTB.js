import React from 'react'
import { Fade, Bounce, Flip, Slide } from "react-awesome-reveal";
import { Link as ScrollLink, animateScroll as scroll } from "react-scroll";
// import calculator from "../../pages/workShop/natpacanSri/calculator"

function workshopTB() {

    const  workshops = [
        {
            name:"Workshop-resume",
            date:"16/11/2023",
            link:"-",
            path:"./natpacanSri/resume"
        },
        {
            name:"Workshop-react(now)",
            date:"16/11/2023",
            link:"https://gitlab.com/NatpacanSri/workshop-react.git",
        },
        {
            name:"Workshop-calculator",
            date:"17/11/2023",
            link:"https://gitlab.com/NatpacanSri/workshop-react.git",
            path:"./natpacanSri/calculator"
        },
        {
            name:"Workshop2",
            date:"17/11/2023",
            link:"https://gitlab.com/NatpacanSri/workshop-react.git",
            path:"./natpacanSri/workshop2"
        },
    ]

    return (
        <div id='workshopTB' className='py-48'>

            {/* title */}
            <Slide triggerOnce direction='up' duration={1800} className='my-8 text-2xl font-bold text-gray-600'>
                <h2  >
                    Workshop Table
                </h2>
            </Slide>

            {/* table */}
            <Fade triggerOnce duration={2000} className="relative overflow-x-auto shadow-md sm:rounded-lg">
                <table className="w-full text-sm text-left rtl:text-right text-gray-500 ">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-100 ">
                        <tr>
                            <th className="px-6 py-3">
                                Workshop name
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Date
                            </th>
                            <th scope="col" className="px-6 py-3">
                                Git
                            </th>
                            <th scope="col" className="px-6 py-3">
                                visit
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {workshops.map((workshop)=>(
                            <tr className="bg-white border-b ">
                                <th className="px-6 py-4 font-semibold text-gray-900 whitespace-nowrap">
                                    {workshop.name}
                                </th>
                                <td className="px-6 py-4">
                                    {workshop.date}
                                </td>
                                <td className="px-6 py-4">
                                    <a target='_blank' href={workshop.link}>{workshop.link}</a>
                                </td>
                                <td className="px-6 py-4 text-indigo-600">
                                    <a target='_blank' href={workshop.path}>go</a>
                                </td>
                            </tr>
                        ))}
                        
                    </tbody>
                </table>
            </Fade>
            
            <div className="mt-5 flex">
                <div className="ml-auto">
                    <ScrollLink
                        to="aboutme"
                        smooth={true}
                        duration={1000}
                        className="text-sm font-semibold leading-6 text-indigo-600 cursor-pointer">
                        about me <span aria-hidden="true">→</span>
                    </ScrollLink>
                </div>
            </div>
            
        </div>

    )
}

export default workshopTB