import React from 'react'

import {
    Box,
    Grid,
    Typography,
    List,
    ListItem,
    TextField,
    Button,
    Container
} from '@mui/material'
import {
    LocationOn, Email
} from '@mui/icons-material';

export default function footer() {

    const menuItems = ['Homepage', 'About Us', 'Services', 'Projects'];

    return (
        <>
            {/* <Box
                sx={{
                    backgroundColor:'white',

                }}
            >
            
            </Box> */}
            <Container maxWidth="xl"
                border={1}
                sx={{
                    backgroundColor:'#148fb2',
                    color:'white',
                    paddingBottom:'30px',
                    paddingTop:'30px',
                    position:'relative',
                    transform: 'translateY(55%)',
                    zIndex:'20',
                }}
            >
                <Container maxWidth="lg">
                    <Typography
                        sx={{
                            fontSize:'3.5em',
                            fontWeight:'900',
                            letterSpacing:".15em"
                        }}
                    >
                        LETS CHANGE YOUR OWN HOME <br/> INTERIOR DESIGN NOW
                    </Typography>
                    <Button variant="contained" sx={{ marginTop: '15px', borderRadius: "8px",padding:'13px 23px', backgroundColor:'#146C94',  }}>
                        CONTACT US
                    </Button>
                </Container>
                
            </Container>
            <Box
                sx={{
                    padding: "40px 0",
                    paddingRight: "24px",
                    paddingLeft: "24px",
                    backgroundColor: '#146C94',
                    color: "white",
                    paddingTop:'180px',
                    // position:'absolute',
                    // width:"100%",
                    // zIndex:'10'
                }}
            >
                <Grid container
                    // spacing={{ xs: 2, md: 3, xl:4 }}
                    columns={{ xs: 4, sm: 8, xl: 12 }}
                >
                    <Grid item
                        md={7}
                    >
                        <Typography
                            // variant='h6'
                            sx={{
                                fontSize: '1.25em',
                                fontWeight: "900",
                                marginBottom: '10px'
                            }}
                        >INFORMATION</Typography>
                        <Typography
                            width='60%'
                        >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Typography>
                    </Grid>
                    <Grid item
                        md={2}
                    >
                        <Typography
                            sx={{
                                fontSize: '1.25em',
                                fontWeight: "900",
                                marginBottom: '10px'
                            }}
                        >NAVIGATION</Typography>
                        <List sx={{ padding: 0, margin: 0 }}>
                            {menuItems.map((item, index) => (
                                <ListItem key={index} sx={{ margin: '0', padding: '0' }} >
                                    <Typography sx={{ margin: '0', padding: '0' }}>{`> ${item}`}</Typography>
                                </ListItem>
                            ))}
                        </List>
                    </Grid>
                    <Grid item
                        md={3}
                    >
                        <Typography
                            sx={{
                                fontSize: '1.25em',
                                fontWeight: "900",
                                marginBottom: '10px'
                            }}
                        >CONTACT US</Typography>
                        <List sx={{ padding: 0, margin: 0 }}>
                            <ListItem sx={{ margin: '0', padding: '0' }} >
                                <LocationOn sx={{ marginRight: '7px' }} /> Lumbung Hidup East Java
                            </ListItem>
                            <ListItem sx={{ margin: '0', padding: '0' }} >
                                <Email sx={{ marginRight: '7px' }} /> Hello@Homco.com
                            </ListItem>
                        </List>
                        <Grid container
                            sx={{ marginTop: '10px' }}>
                            <Grid item>
                                <TextField
                                    id="filled-basic"
                                    label="Email Address"
                                    variant="filled"
                                    sx={{
                                        flex: 1,
                                        backgroundColor: 'white',
                                        borderRadius: '8px',
                                        border: 'lightgray 1.5px solid',
                                        marginRight: '10px'
                                    }}
                                />
                            </Grid>
                            <Grid item>
                                <Button variant="contained"
                                    sx={{
                                        margin: '0',
                                        height: '100%',
                                    }}
                                >SUBSCRIBE</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Box>
        </>

    )
}
