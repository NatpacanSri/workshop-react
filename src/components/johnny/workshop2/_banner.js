import React from 'react'

export default function banner({titleName}) {
    return (
        <div className='bg-[#98b2ff] min-w-full p-[24px] py-28 border-t border-gray-50 relative z-20'>
            <div className="w-40 h-40 rounded-full border-[16px] border-[#bbb4ff] absolute -top-0 -z-10"></div>
            <div className='z-10 absolute min-h-[300px] h-[300px]'>
                <h1 className='font-bold text-6xl text-white mb-5'>{titleName}</h1>
                <p className='text-white font-semibold w-[36rem]'>lorst amet, consectetur ad p sc ng el t. Ut el t tellus, uctus
                    psunec ullamcor er mattis, ulvinar dapibus leo.</p>
            </div>
        </div>
    )
}
