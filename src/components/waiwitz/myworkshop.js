import * as React from 'react';
import styles from "src/styles/waiwitzStyles.module.css";
import { Box, Button, Card, CardHeader, CardContent, Container, Typography, Paper, Grid, Link } from "@mui/material";

const workshop = [
    {
        name: 'Workshop 1 Resume 🧾',
        link: 'waiwitz/resumeAboutMe'
    },
    {
        name: 'Workshop 2 Calculator 🧮',
        link: 'waiwitz/calculator'
    }
]

const MyWorkshop = () => {
    const [value, setValue] = React.useState(0);
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <>
            <Container maxWidth='xl' className={styles.spaceContainer}>
                <Card>
                    <CardHeader title='Workshop 💪' />
                    <CardContent>
                        <Grid container spacing={3}>
                            {workshop.map(((work) => (
                                <Grid item key={work.name} >
                                    <Link href={work.link}>
                                        <Button style={{ backgroundColor: '#eee', color: '#001' }}>
                                            {work.name}
                                        </Button>
                                    </Link>
                                </Grid>
                            )))}
                        </Grid>
                    </CardContent>
                </Card>
            </Container>
        </>
    )
}


export default MyWorkshop;