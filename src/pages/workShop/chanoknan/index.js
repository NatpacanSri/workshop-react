import Navbar from "../../../components/miw/Navbar";
import Footer from "../../../components/miw/Footer";
import { IconButton } from "@mui/material";
import MusicNoteIcon from "@mui/icons-material/MusicNote";
import { Card, CardMedia, Typography, Link } from "@mui/material";
import { Layout } from "src/layouts/dashboard/layout";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";


const chanoknan = () => {
  return (
    <>
      <Navbar />
      <div style={{ marginTop: "10px", padding: "10px", margin: "auto", display: "block" }}>
        <Card>
          {/* URL ของภาพ */}
          <CardMedia
            component="img"
            height="50"
            image="https://scontent.fbkk29-5.fna.fbcdn.net/v/t1.6435-9/157239596_2286349744841690_6783253182747429945_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=be3454&_nc_eui2=AeFFV2KzVIh_kJp1oSkP_T_RsfGImDfaGxOx8YiYN9obEwt23RluVXQx6xGUK2W_LjNr4Sd2TbS2X9Mx5Baa6G9y&_nc_ohc=cgsdF4H1upUAX_YxNwI&_nc_ht=scontent.fbkk29-5.fna&oh=00_AfBQ_ZqYzthcwB8wYaoJHe4J0Ofzry1G9_TXKdIvqu_T-g&oe=657D013A" // แทนที่ URL_ของภาพ.jpg ด้วยที่อยู่ URL ของรูปภาพที่คุณต้องการแสดง
            alt="profile"
            sx={{
              borderRadius: "50%",
              width: "300px",
              height: "300px",
              margin: "auto",
              display: "block",
            }} // ปรับรูปให้มีวงรี
          />
        </Card>
      </div>

      <Typography
        variant="h1"
        sx={{ color: "#2E86C1 ", margin: "auto", display: "block" }}
        className="text-6xl "
      >
        Chanoknan U
      </Typography>
      <div className="text-sm text-center ">
        <Typography variant="body1" sx={{ marginBottom: 2 }}>
          Hello, I'm Chanoknan Hirunnukhor, also known as "Mew." I am currently a fourth-year
          university student at Mahidol University. I am currently undergoing an internship at TCC
          Technology. My family, consisting of four members, is a source of warmth and encouragement
          for me. Family is crucial in my life, providing support and motivation for personal
          development. During my time at university and in my internship at TCC Technology, I have
          gained valuable experience in the field of information technology. It has been an exciting
          and enjoyable journey, contributing to my overall growth. I am dedicated to developing my
          skills and knowledge in various aspects to benefit both myself and society. My goal is to
          have a successful and fulfilling future, growing in every dimension of life.
        </Typography>
        <Typography variant="body1" sx={{ marginBottom: 2 }}>
          If you have any questions or would like additional information about me, feel free to ask!
          👉👉
          <Link
            color="inherit"
            href="https://www.facebook.com/profile.php?id=100004000597236&locale=th_TH"
          >
            facebook
          </Link>{" "}
        </Typography>
        <div className="text-center">
          <Box sx={{ width: "50%", margin: "auto", padding: "20px" }}>
            <LinearProgress
              sx={{
                backgroundColor: "lightgray",
                "& .MuiLinearProgress-bar": { backgroundColor: "green" },
              }}
            />
          </Box>
        </div>
        <div className="">
          <Box display="flex" flexDirection="row" justifyContent="space-between">
            <Box
              width="48%"
              sx={{
                backgroundColor: "#ff95b4 ",
                margin: "10px",
                padding: "10px",
                borderRadius: "8px",
                display: "block",
              }}
            >
              <Typography variant="body1" sx={{ color: "white" }}>
                <Link
                  href="https://open.spotify.com/track/19vHgVS1aukRiQWhTqfKnE?si=3f23d7facc52462f"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <IconButton>
                    <MusicNoteIcon />
                  </IconButton>
                  My Favorite Song
                </Link>
              </Typography>
            </Box>
            <Box
              width="48%"
              sx={{
                backgroundColor: "#ff95b4 ",
                margin: "10px",
                padding: "10px",
                borderRadius: "8px",
                display: "block",
              }}
            >
              <Typography variant="body1" sx={{ color: "white" }}>
                <Link href="/workShop/chanoknan/cal">Calculator</Link>
              </Typography>
            </Box>
          </Box>
          {/* <div>
            <h1>Calculator App</h1>
            <Calculator />
          </div> */}
        </div>
      </div>

      <Footer />
    </>
  );
};
chanoknan.getLayout = (chanoknan) => <Layout>{chanoknan}</Layout>;
export default chanoknan;
