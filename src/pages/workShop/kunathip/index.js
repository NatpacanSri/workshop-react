import { Layout } from "src/layouts/dashboard/layout";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Image from "next/image";
import Link from "next/link";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
const getImagePath = (imageName) => {
  return `/imageknt/${imageName}`;
};

const kunathip = () => {
  // Personal information
  const personalInfo = {
    name: "Kunathip Padthaisong",
    address: "130/1 Chaiyaphum city",
    phone: "0921674344",
    email: "kunathip@kkumail.com",
  };
  const profileImagePath = getImagePath("Profile.jpg");

  return (
    <>
      <Box sx={{ flexGrow: 1, backgroundColor: "#303841", padding: "1em" }}>
        <Grid container spacing={2}>
          <Grid item xs={5} sx={{ color: "white", marginTop: "15em", marginLeft: "10em" }}>
            <h2 style={{ color: "#FFFFF", textAlign: "left" }}>Hello, I am</h2>
            <h1 style={{ color: "#FFFFF", textAlign: "left" }}>Kunathip</h1>
            <p style={{ color: "#FFFFF" }}>
              {}
              Hello! I'm Por, a seasoned programmer with expertise in software and web application
              development. With experience in various businesses and projects, I am dedicated to
              creating efficient and user-centric applications.
            </p>

            <div>
              <h1 style={{ color: "#FFFFF", textAlign: "left" }}>My workshop</h1>
              <p style={{ color: "black" }}>
                <Link href="/workShop/kunathip/workshop-calculator">
                  <Button variant="outlined">Workshop 1</Button>
                </Link>
              </p>
            </div>
          </Grid>
          <Grid item xs={5} sx={{ marginTop: "150px", alignItems: "center", marginLeft: "auto" }}>
            <Image
              style={{ margin: "15px", borderRadius: "50%", boxShadow: "5px 10px 450px #6366F1" }}
              src={profileImagePath}
              alt="Profile"
              width={445}
              height={445}
            />
          </Grid>
          <Grid item xs={5} sx={{ color: "white", marginLeft: "10em" }}></Grid>
        </Grid>
        <Grid>

        </Grid>
      </Box>
    </>
  );
};

kunathip.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;
export default kunathip;
