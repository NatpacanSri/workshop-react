import { Container } from "@mui/system";
import { useState } from "react";
import { Layout } from "src/layouts/dashboard/layout";

const kunathip = () => {
  const [result, setResult] = useState("");
  const [input, setInput] = useState("");

  const handleButtonClick = (value) => {
    setInput((prevInput) => prevInput + value);
  };

  const handleClear = () => {
    setInput("");
    setResult("");
  };

  const handleCalculate = () => {
    try {
      setResult(eval(input).toString());
    } catch (error) {
      setResult("Error");
    }
  };

  // สไตล์สำหรับปุ่มที่เป็นวงกลม
  const roundButtonStyle = {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    margin: "5px",
    fontSize: "16px",
  };

  return (
    <div style={{ alignItems: "center", justifyContent: "center" }}>
      <Container
        maxWidth="sm"
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          border: "2px solid #427D9D",
          width: "350px",
          orderRadius: "10px",
          borderRadius: "1em",
          boxShadow: "5px 10px 10px #888888",
          backgroundColor: "#427D9D",
        }}
      >
        <div style={{ color: "#FFFFFF" }}>
          <h2>Calculator</h2>

          <p>Result: {result}</p>
        </div>
        <div>
          <input type="text" value={input} readOnly />
        </div>
        <div style={{ padding: "1em" }}>
          <div>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("7")}>
              7
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("8")}>
              8
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("9")}>
              9
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("/")}>
              /
            </button>
          </div>
          <div>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("4")}>
              4
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("5")}>
              5
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("6")}>
              6
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("*")}>
              *
            </button>
          </div>
          <div>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("1")}>
              1
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("2")}>
              2
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("3")}>
              3
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("-")}>
              -
            </button>
          </div>
          <div>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("0")}>
              0
            </button>
            <button style={roundButtonStyle} onClick={handleClear}>
              C
            </button>
            <button style={roundButtonStyle} onClick={handleCalculate}>
              =
            </button>
            <button style={roundButtonStyle} onClick={() => handleButtonClick("+")}>
              +
            </button>
          </div>
        </div>
      </Container>
    </div>
  );
};

kunathip.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;
export default kunathip;
